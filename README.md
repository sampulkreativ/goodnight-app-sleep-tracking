# README
Before running the program, you need to migrate first with `rails db:migrate`, then you can do `rails db:seed` to get the required dummy data. Then you can run the server use `rails server`


# Endpoint Good Night App

## Login User Endpoint
```
POST # localhost:3000/api/users/login
{
    "email": "user1@example.com",
    "password": "password"
}
```

Response for Login User
```json
{
    "status": 200,
    "token": "[jwt_token]"
}
```

## Start Sleep
```
POST # localhost:3000/api/users
```
Response for Start Sleep
```json
{
    "status": 201,
    "data": {
        "id": 1,
        "sleep_status": true,
        "start_time": "2023-06-09T10:25:28.946Z",
        "end_time": null,
        "duration": 0,
        "user_id": 2,
        "created_at": "2023-06-09T10:25:28.953Z",
        "updated_at": "2023-06-09T10:25:28.953Z"
    }
}
```
## End Sleep
```
PUT # localhost:3000/api/users
```

Response API for End Sleep
```json
{
    "status": 200,
    "data": {
        "duration": 102,
        "sleep_status": false,
        "end_time": "2023-06-09T10:27:10.036Z",
        "id": 1,
        "start_time": "2023-06-09T10:25:28.946Z",
        "user_id": 2,
        "created_at": "2023-06-09T10:25:28.953Z",
        "updated_at": "2023-06-09T10:27:10.039Z"
    }
}
```

## Follow User
```
POST # localhost:3000/api/users/2/follow
```
Response API for Follow User
```json
{
    "status": 200,
    "message": "User followed successfully"
}
```

## Unfollow User
```
DELETE # localhost:3000/api/users/2/unfollow
```
Response API for Unfollow User
```json
{
    "status": 200,
    "message": "User unfollowed successfully"
}
```

## Show Sleeping Records from followed user
```
GET # localhost:3000/api/sleeping_records
```
Response from Show Sleeping Records
```json
{
    "status": 200,
    "data": [
        {
            "id": 1,
            "sleep_status": false,
            "start_time": "2023-06-02T17:00:00.000Z",
            "end_time": "2023-06-02T22:00:00.000Z",
            "duration": 18000,
            "user_id": 1,
            "created_at": "2023-06-02T17:00:00.000Z",
            "updated_at": "2023-06-02T22:00:00.000Z",
            "user": {
                "id": 1,
                "name": "User 1",
                "email": "user1@example.com"
            }
        }
    ]
}
```

# Additional Feature

## Show All Users
```
GET # localhost:3000/api/users
```
Response for Show All Users
```json
{
    "status": 200,
    "users": [
        {
            "created_at": "2023-06-09T09:05:43.938Z",
            "email": "user1@example.com",
            "id": 1,
            "name": "User 1",
            "updated_at": "2023-06-09T09:05:43.938Z",
            "relationId": null
        },
        {
            "created_at": "2023-06-09T09:05:44.142Z",
            "email": "user2@example.com",
            "id": 2,
            "name": "User 2",
            "updated_at": "2023-06-09T09:05:44.142Z",
            "relationId": null
        },
        {
            "created_at": "2023-06-09T09:05:44.342Z",
            "email": "user3@example.com",
            "id": 3,
            "name": "User 3",
            "updated_at": "2023-06-09T09:05:44.342Z",
            "relationId": null
        },
        {
            "created_at": "2023-06-09T09:05:44.540Z",
            "email": "user4@example.com",
            "id": 4,
            "name": "User 4",
            "updated_at": "2023-06-09T09:05:44.540Z",
            "relationId": null
        }
    ],
    "current_user": {
        "id": 3,
        "name": "User 3",
        "email": "user3@example.com",
        "created_at": "2023-06-08T03:19:01.161Z",
        "updated_at": "2023-06-08T03:19:01.161Z"
    }
}
```

## Show an User
```
GET # localhost:3000/api/users/1
```
Response for Show an User
```json
{
    "status": 200,
    "user": {
        "created_at": "2023-06-09T09:05:43.938Z",
        "email": "user1@example.com",
        "id": 1,
        "name": "User 1",
        "updated_at": "2023-06-09T09:05:43.938Z",
        "relationId": 1
    },
    "current_user": {
        "id": 3,
        "name": "User 3",
        "email": "user3@example.com",
        "created_at": "2023-06-08T03:19:01.161Z",
        "updated_at": "2023-06-08T03:19:01.161Z"
    }
}
```