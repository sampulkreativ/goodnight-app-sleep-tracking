class Api::RelationshipsController < ApplicationController
  before_action :authenticate_request!
  before_action :is_already_follow?, only: [:create, :destroy]
  def create
    return render json: { status: 404, error: 'User not found' }, status: 404 if @user.nil?
    return render json: { status: 400, error: "You are already following #{@user.name}" }, status: 400 if is_already_follow?
    return render json: { status: 400, error: "You cannot unfollow/follow yourself" }, status: 400 if @user.id == current_user_id

    current_user.followeds << @user

    render json: { status: 200, message: 'User followed successfully' }, status: 200
  end

  def destroy

    return render json: { status: 400, error: "You cannot unfollow/follow yourself" }, status: 400 if @user.id == current_user_id
    return render json: {status: 404, error: "You are not following #{@user.name}"}, status: 404 unless is_already_follow?

    return render json: { status: 200, message: 'User unfollowed successfully' }, status: 200 if current_user.followeds.destroy(@user)
  end

  private
  def is_already_follow?
    @user = User.find_by_id(params[:id])
    relation = Relationship.where(follower_id: current_user_id, followed_id: @user.id)
    return relation.exists?
  end
end
