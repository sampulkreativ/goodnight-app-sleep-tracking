require 'sleeping_record'
class Api::SleepingRecordsController < ApplicationController
  before_action :authenticate_request!
  before_action :find_user, only: :index
  def index
    if !current_user.followeds
      return render json: {status: 401, error: 'You are not following anyone'}, status: 401
    else
      followed_users = current_user.followeds

      start_date = Date.today.at_beginning_of_week - 7.days
      end_date = Date.today.at_beginning_of_week

      sleep_records = SleepingRecord.includes(:user).where(user_id: followed_users.pluck(:id), end_time: start_date..end_date).order(duration: :desc)

      render json: {status: 200, data: sleep_records.as_json(include: {user: {only: [:id, :email, :name]}})}, status: 200
    end

  end

  def create
    last_sleep_status = SleepingRecord.where(user_id: current_user_id).order(id: :desc).limit(1).pluck('sleep_status').first

    return render json: {status: 500, error: 'You are already sleeping'}, status: 500 if last_sleep_status == true

    @sleep_record = SleepingRecord.new(user_id: current_user_id, sleep_status: true, start_time: Time.now, duration: 0)
    if @sleep_record.save!
      return render json: {status: 201, data: @sleep_record}, status: 201
    end
  end

  def update
    last_sleep = SleepingRecord.where(user_id: current_user_id).order(id: :desc).limit(1).first
    last_sleep_status = last_sleep.sleep_status

    return render json: {status: 500, error: 'You are not sleeping'}, status: 500 if last_sleep_status == false

    @sleep_record = last_sleep
    return render json: {status: 404, error: 'Data not found'}, status: 404 if @sleep_record.nil?

    return render json: {
      status: 400,
      error: 'Your sleeping records has been finished, cannot be update'
    }, status: 400 if !@sleep_record.sleep_status

    duration = Time.now.to_i - @sleep_record.start_time.to_i
    if @sleep_record.update(duration: duration, sleep_status: false, end_time: Time.now)
      return render json: {status: 200, data: @sleep_record}, status: 200
    end
  end

  private
    def find_user
      @user ||=User.find(params[:user_id])
    end

end
