class Api::UsersController < ApplicationController
  before_action :authenticate_request!, except:[:create, :login]

  def index
    users = User.all_except(current_user).map do |item|

      new_item = {
        created_at: item.created_at,
        email: item.email,
        id: item.id,
        name: item.name,
        updated_at: item.updated_at,
        relationId: current_user.following?(item) ? current_user.followed_relationships.find_by(followed_id: item.id).id : nil,
      }
      new_item
    end

    render json: {
      status: 200,
      users: users,
      current_user: current_user.as_json(except: :password_digest)
    }
  end

  def show
    user = User.find(params[:id])

    render json: {
      status: 200,
      user: {
        created_at: user.created_at,
        email: user.email,
        id: user.id,
        name: user.name,
        updated_at: user.updated_at,
        relationId: current_user.following?(user) ? current_user.followed_relationships.find_by(followed_id: user.id).id : nil,
      },
      current_user: current_user.as_json(except: :password_digest)
    }
  end

  def login
    email = params[:email]
    password = params[:password]

    user = User.find_by(email: params[:email])
    return render json: {error: 'User Not Found'} unless user

    jwt_token = User.authenticate(email, password)

    if(jwt_token)
      render json: {status:200, token: jwt_token}
    else
      render json: {status: 401,error: 'invalid credentials'}, status: 401
    end
  end

  private

  def user_login_params
    params.permit(:email, :password)
  end

  def user_params
    params.permit(:name, :email, :password, :password_confirmation)
  end

end
