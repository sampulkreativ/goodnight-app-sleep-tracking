class User < ApplicationRecord
  has_secure_password

  scope :all_except, ->(user) { where.not(id: user) }

  has_many :sleeping_records
  has_many :follower_relationships, foreign_key: :followed_id, class_name: 'Relationship'
  has_many :followers, through: :follower_relationships, source: :follower_user

  has_many :followed_relationships, foreign_key: :follower_id, class_name: 'Relationship'
  has_many :followeds, through: :followed_relationships, source: :followed_user


  def self.authenticate(email, password)
    user = find_by(email: email)
    jwt_payload = {user_id: user.id}
    jwt_token = JWT.encode(jwt_payload, Rails.application.secrets.secret_key_base)
  end

  def following?(other_user)
    followeds.exists?(other_user.id)
  end

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :password, presence: true, length: { minimum: 6 }
end
