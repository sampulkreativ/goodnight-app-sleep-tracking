Rails.application.routes.draw do
  namespace :api do
    get 'sleep', to: 'sleeping_records#index', as: 'index_sleep'
    post 'sleep', to: 'sleeping_records#create', as: 'sleep'
    put 'sleep', to: 'sleeping_records#update', as: 'unsleep'
    resources :users, only: [:index, :show]
    post 'users/login', to: 'users#login', as: 'login_user'
    post 'users/:id/follow', to: 'relationships#create', as: 'follow_user'
    delete 'users/:id/unfollow', to: 'relationships#destroy', as: 'unfollow_user'
  end
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
