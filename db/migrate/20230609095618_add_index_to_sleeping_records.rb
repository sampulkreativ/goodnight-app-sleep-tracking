class AddIndexToSleepingRecords < ActiveRecord::Migration[7.0]
  def change
    add_index :sleeping_records, :user_id
  end
end
