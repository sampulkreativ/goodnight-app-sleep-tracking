class CreateSleepingRecords < ActiveRecord::Migration[7.0]
  def change
    create_table :sleeping_records do |t|
      t.boolean :sleep_status, null: false
      t.datetime :start_time, null: false
      t.datetime :end_time, null: true
      t.integer :duration, null: false
      t.integer :user_id, null: false

      t.timestamps
    end
  end
end
