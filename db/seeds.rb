# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

5.times do |i|
  name = "User #{i}"
  email = "user#{i}@example.com"
  password = "password"
  User.create!(
    name: name,
    email: email,
    password: password,
    password_confirmation: password
  )
end

puts 'User created successfully'

SleepingRecord.create!(
  sleep_status: false,
  start_time: Time.new(2023, 6, 1),
  end_time: Time.new(2023, 6, 2, 7),
  duration: Time.new(2023, 6, 2, 7).to_i - Time.new(2023, 6, 1).to_i,
  user_id: 2,
  created_at: Time.new(2023, 6, 1),
  updated_at: Time.new(2023, 6, 2, 7)
)
SleepingRecord.create!(
  sleep_status: false,
  start_time: Time.new(2023, 5, 30, 8),
  end_time: Time.new(2023, 5, 31, 5),
  duration: Time.new(2023, 5, 31, 5).to_i - Time.new(2023, 5, 30).to_i,
  user_id: 2,
  created_at: Time.new(2023, 5, 30),
  updated_at: Time.new(2023, 5, 31, 5)
)
SleepingRecord.create!(
  sleep_status: false,
  start_time: Time.new(2023, 6, 1),
  end_time: Time.new(2023, 6, 2, 8),
  duration: Time.new(2023, 6, 2, 8).to_i - Time.new(2023, 6, 1).to_i,
  user_id: 3,
  created_at: Time.new(2023, 6, 1),
  updated_at: Time.new(2023, 6, 2, 8)
)

puts 'SleepingRecord created successfully'

Relationship.create!(
  follower_id: 1,
  followed_id: 2
)
Relationship.create!(
  follower_id: 1,
  followed_id: 3
)

puts 'Relationship created successfully'
